<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set('America/New_York');

define('BASEPATH','runthisshit');
require_once('HealthService.php');

$oHS = new HealthService();
$aResults = $oHS->runAllTests();
echo json_encode($aResults);
